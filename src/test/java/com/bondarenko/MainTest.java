package com.bondarenko;

import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockStatic;

class MainTest {

//    private static MockedStatic<LoggerFactory> mockedLogger;
    private Main main;

//    @BeforeAll
//    public static void hideLogMessages() {
//        mockedLogger = mockStatic(LoggerFactory.class);
//        mockedLogger.when(() -> LoggerFactory.getLogger(Main.class)).thenReturn(Mockito.mock(Logger.class));
//    }
//
//    @AfterAll
//    public static void deleteMock() {
//        mockedLogger.close();
//    }

    @BeforeEach
    void setUp() {
        BigDecimal min = BigDecimal.valueOf(1);
        BigDecimal max = BigDecimal.valueOf(9);

        // correct increment
        BigDecimal increment = BigDecimal.valueOf(2);

        main = new Main(min, max, increment, Type.INT);
    }

    @AfterEach
    void tearDown() {
        main = null;
    }

    @Test
    void isValuesCorrect() {
        assertTrue(main.isValuesCorrect());

        BigDecimal min = BigDecimal.valueOf(1);
        BigDecimal max = BigDecimal.valueOf(9);

        //incorrect increment
        BigDecimal increment = BigDecimal.valueOf(-2);
        assertFalse(new Main(min, max, increment, Type.BYTE).isValuesCorrect());
    }

    @Test
    void getMultiplicationEquationWithCorrectValues() {
        BigDecimal first = BigDecimal.valueOf(12);
        BigDecimal second = BigDecimal.valueOf(3);
        String result = "12 * 3 = 36";
        assertEquals(result, main.getMultiplicationEquation(first, second));
    }

    @Test
    void getMultiplicationEquationWithNull() {
        BigDecimal value = BigDecimal.valueOf(3);
        assertEquals("", main.getMultiplicationEquation(value, null));
        assertEquals("", main.getMultiplicationEquation(null, value));
    }
}