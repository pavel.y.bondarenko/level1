package com.bondarenko;

import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockStatic;

class ConverterTest {

//    private static MockedStatic<LoggerFactory> mockedLogger;
    private Converter converter;

//    @BeforeAll
//    public static void hideLogMessages() {
//        mockedLogger = mockStatic(LoggerFactory.class);
//        mockedLogger.when(() -> LoggerFactory.getLogger(Converter.class)).thenReturn(Mockito.mock(Logger.class));
//    }
//
//    @AfterAll
//    public static void deleteMock() {
//        mockedLogger.close();
//    }

    @BeforeEach
    void setUp() {
        converter = new Converter();
    }

    @AfterEach
    void tearDown() {
        converter = null;
    }

    @Test
    void convertByteTest() {
        BigDecimal byteMaxValue = BigDecimal.valueOf(Byte.MAX_VALUE);
        BigDecimal byteMinValue = BigDecimal.valueOf(Byte.MIN_VALUE);

        assertEquals(0, converter.convert(byteMaxValue.toString(), Type.BYTE).compareTo(byteMaxValue));
        assertEquals(0, converter.convert(byteMinValue.toString(), Type.BYTE).compareTo(byteMinValue));

        assertNull(converter.convert(byteMaxValue.add(BigDecimal.ONE).toString(), Type.BYTE));
        assertNull(converter.convert(byteMinValue.subtract(BigDecimal.ONE).toString(), Type.BYTE));
    }

    @Test
    void convertShortTest() {
        BigDecimal shortMaxValue = BigDecimal.valueOf(Short.MAX_VALUE);
        BigDecimal shortMinValue = BigDecimal.valueOf(Short.MIN_VALUE);

        assertEquals(0, converter.convert(shortMaxValue.toString(), Type.SHORT).compareTo(shortMaxValue));
        assertEquals(0, converter.convert(shortMinValue.toString(), Type.SHORT).compareTo(shortMinValue));

        assertNull(converter.convert(shortMaxValue.add(BigDecimal.ONE).toString(), Type.SHORT));
        assertNull(converter.convert(shortMinValue.subtract(BigDecimal.ONE).toString(), Type.SHORT));
    }

    @Test
    void convertIntTest() {
        BigDecimal intMaxValue = BigDecimal.valueOf(Integer.MAX_VALUE);
        BigDecimal intMinValue = BigDecimal.valueOf(Integer.MIN_VALUE);

        assertEquals(0, converter.convert(intMaxValue.toString(), Type.INT).compareTo(intMaxValue));
        assertEquals(0, converter.convert(intMinValue.toString(), Type.INT).compareTo(intMinValue));

        assertNull(converter.convert(intMaxValue.add(BigDecimal.ONE).toString(), Type.INT));
        assertNull(converter.convert(intMinValue.subtract(BigDecimal.ONE).toString(), Type.INT));
    }

    @Test
    void convertLongTest() {
        BigDecimal longMaxValue = BigDecimal.valueOf(Long.MAX_VALUE);
        BigDecimal longMinValue = BigDecimal.valueOf(Long.MIN_VALUE);

        assertEquals(0, converter.convert(longMaxValue.toString(), Type.LONG).compareTo(longMaxValue));
        assertEquals(0, converter.convert(longMinValue.toString(), Type.LONG).compareTo(longMinValue));

        assertNull(converter.convert(longMaxValue.add(BigDecimal.ONE).toString(), Type.LONG));
        assertNull(converter.convert(longMinValue.subtract(BigDecimal.ONE).toString(), Type.LONG));
    }

    @Test
    void convertFloatTest() {
        String floatMaxValue = String.valueOf(Float.MAX_VALUE);
        String floatMinValue = String.valueOf(-Float.MAX_VALUE);

        assertEquals(0, converter.convert(floatMaxValue, Type.FLOAT).compareTo(new BigDecimal(floatMaxValue)));
        assertEquals(0, converter.convert(floatMinValue, Type.FLOAT).compareTo(new BigDecimal(floatMinValue)));

        assertNull(converter.convert(new BigDecimal(floatMaxValue).multiply(new BigDecimal("2")).toString(), Type.FLOAT));
        assertNull(converter.convert(new BigDecimal(floatMinValue).multiply(new BigDecimal("2")).toString(), Type.FLOAT));
    }

    @Test
    void convertDoubleTest() {
        String doubleMaxValue = String.valueOf(Double.MAX_VALUE);
        String doubleMinValue = String.valueOf(-Double.MAX_VALUE);

        assertEquals(0, converter.convert(doubleMaxValue, Type.DOUBLE).compareTo(new BigDecimal(doubleMaxValue)));
        assertEquals(0, converter.convert(doubleMinValue, Type.DOUBLE).compareTo(new BigDecimal(doubleMinValue)));

        assertNull(converter.convert(new BigDecimal(doubleMaxValue).multiply(new BigDecimal("2")).toString(), Type.DOUBLE));
        assertNull(converter.convert(new BigDecimal(doubleMinValue).multiply(new BigDecimal("2")).toString(), Type.DOUBLE));
    }

    @Test
    void convertIllegalArgumentTest() {
        assertNull(converter.convert("value", Type.INT));
        assertNull(converter.convert("not a number", Type.DOUBLE));
    }
}