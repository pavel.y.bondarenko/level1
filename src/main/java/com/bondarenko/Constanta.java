package com.bondarenko;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public final class Constanta {

    /**
     * A name of file with properties
     */
    public static final String PROPERTY_FILE_NAME = "app.properties";

    /**
     * A name of property with min value
     */
    public static final String PROPERTY_MIN_NAME = "min";

    /**
     * A name of property with max value
     */
    public static final String PROPERTY_MAX_NAME = "max";

    /**
     * A name of property with increment value
     */
    public static final String PROPERTY_INCREMENT_NAME = "increment";

    /**
     * A name of property with username
     */
    public static final String PROPERTY_TYPE_NAME = "type";

    /**
     * default type
     */
    public static final Type DEFAULT_TYPE = Type.INT;

    /**
     * An encoding for property file
     */
    public static final Charset PROPERTY_ENCODING = StandardCharsets.UTF_8;

    /**
     * start multiplier in table
     */
    public static final BigDecimal START_MULTIPLIER = BigDecimal.ONE;

    /**
     * last multiplier in table
     */
    public static final BigDecimal LAST_MULTIPLIER = new BigDecimal("9");

    private Constanta() {
    }
}
