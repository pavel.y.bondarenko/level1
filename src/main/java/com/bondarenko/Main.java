package com.bondarenko;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Properties;

import static com.bondarenko.Constanta.*;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    private static final PropertyLoader PROPERTY_LOADER = new PropertyLoader();
    private static final Converter CONVERTER = new Converter();
    private static final String NO_PROPERTY_TEMPLATE = "There is no \"{}\" property";

    private final Type type;
    private BigDecimal current;
    private final BigDecimal max;
    private final BigDecimal increment;
    private BigDecimal multiplier;

    public Main(BigDecimal min,
                BigDecimal max,
                BigDecimal increment,
                Type type) {
        this.current = min;
        this.max = max;
        this.increment = increment;
        this.type = type;
        this.multiplier = START_MULTIPLIER;
    }

    public static void main(String[] args) {
        Properties properties = PROPERTY_LOADER.getProperties(PROPERTY_FILE_NAME);
        if (properties == null) {
            return;
        }

        if (notEnoughProperties(properties)) {
            return;
        }

        Type type = PROPERTY_LOADER.getType();
        BigDecimal min = CONVERTER.convert(properties.getProperty(PROPERTY_MIN_NAME), type);
        BigDecimal max = CONVERTER.convert(properties.getProperty(PROPERTY_MAX_NAME), type);
        BigDecimal increment = CONVERTER.convert(properties.getProperty(PROPERTY_INCREMENT_NAME), type);
        Main program = new Main(min, max, increment, type);

        if (!program.isValuesCorrect()) {
            LOGGER.error("some values incorrect, program has stopped");
            return;
        }

        program.drawTable();
    }

    private static boolean notEnoughProperties(Properties properties) {
        boolean notEnoughProperties = false;

        if (!properties.containsKey(PROPERTY_MIN_NAME)) {
            LOGGER.error(NO_PROPERTY_TEMPLATE, PROPERTY_MIN_NAME);
            notEnoughProperties = true;
        }

        if (!properties.containsKey(PROPERTY_MAX_NAME)) {
            LOGGER.error(NO_PROPERTY_TEMPLATE, PROPERTY_MAX_NAME);
            notEnoughProperties = true;
        }

        if (!properties.containsKey(PROPERTY_INCREMENT_NAME)) {
            LOGGER.error(NO_PROPERTY_TEMPLATE, PROPERTY_INCREMENT_NAME);
            notEnoughProperties = true;
        }

        return notEnoughProperties;
    }

    public boolean isValuesCorrect() {
        boolean isCorrect = true;

        if (this.current == null || this.max == null || this.increment == null) {
            return false;
        }

        if (this.increment.compareTo(BigDecimal.ZERO) == 0) {
            LOGGER.error("increment can not be zero");
            isCorrect = false;
        }

        if (this.current.compareTo(this.max) <= 0 && this.increment.compareTo(BigDecimal.ZERO) < 0) {
            LOGGER.error("with this min and max increment should be positive");
            isCorrect = false;
        }

        if (this.current.compareTo(this.max) >= 0 && this.increment.compareTo(BigDecimal.ZERO) > 0) {
            LOGGER.error("with this min and max increment should be negative");
            isCorrect = false;
        }

        return isCorrect;
    }

    public String getMultiplicationEquation(BigDecimal firstValue,
                                            BigDecimal secondValue) {
        if (firstValue == null || secondValue == null) {
            LOGGER.warn("Missed some value");
            return "";
        }

        BigDecimal product = firstValue.multiply(secondValue);
        try {
            this.type.getConverter().apply(product.toString());
        } catch (NumberFormatException exception) {
            LOGGER.warn("product of {} and {} is out of {} type range", firstValue, secondValue, this.type.name());
        }
        return firstValue + " * " + secondValue + " = " + product;
    }

    private void drawTable() {
        while ((this.increment.compareTo(BigDecimal.ZERO) > 0 && this.current.compareTo(this.max) <= 0)
                || (this.increment.compareTo(BigDecimal.ZERO) < 0 && this.current.compareTo(this.max) >= 0)) {
            while (this.multiplier.compareTo(LAST_MULTIPLIER) <= 0) {
                LOGGER.info(getMultiplicationEquation(this.current, this.multiplier));
                this.multiplier = this.multiplier.add(BigDecimal.ONE);
            }
            LOGGER.info("---");
            this.current = this.current.add(this.increment);
            this.multiplier = START_MULTIPLIER;
        }
    }
}