package com.bondarenko;

import java.util.function.Function;

public enum Type {
    BYTE(Byte::valueOf),
    SHORT(Short::valueOf),
    INT(Integer::valueOf),
    LONG(Long::valueOf),
    FLOAT(Float::valueOf),
    DOUBLE(Double::valueOf);

    private final Function<String, ? extends Number> converter;

    Type(Function<String, ? extends Number> converter) {
        this.converter = converter;
    }

    public Function<String, ? extends Number> getConverter() {
        return converter;
    }
}
