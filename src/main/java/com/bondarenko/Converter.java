package com.bondarenko;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class Converter {
    private static final Logger LOGGER = LoggerFactory.getLogger(Converter.class);

    public BigDecimal convert(String value,
                              Type type) {
        try {
            BigDecimal convertedValue = new BigDecimal(String.valueOf(type.getConverter().apply(value)));
            LOGGER.debug("value = {} converted successfully", convertedValue);
            return convertedValue;
        } catch (NumberFormatException exception) {
            LOGGER.error("incorrect number \"{}\" for type {}", value, type.name(), exception);
            return null;
        }
    }
}
