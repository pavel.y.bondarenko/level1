package com.bondarenko;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

import static com.bondarenko.Constanta.*;

public class PropertyLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyLoader.class);

    public Properties getProperties(String fileName) {
        Properties properties = new Properties();

        File jarFile = new File(PropertyLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        File propertyFile = new File(jarFile.getParent(), fileName);

        try {
            if (propertyFile.exists()) {
                try (FileReader propertyFileReader = new FileReader(propertyFile, PROPERTY_ENCODING)) {
                    properties.load(propertyFileReader);
                }
                LOGGER.debug("External property file loaded - {}", propertyFile.getAbsolutePath());
            } else {
                ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
                InputStream resourceAsStream = classLoader.getResourceAsStream(fileName);
                properties.load(new InputStreamReader(resourceAsStream, PROPERTY_ENCODING));
                LOGGER.debug("Internal property file loaded");
            }
        } catch (IOException exception) {
            LOGGER.warn("property file not found", exception);
            return null;
        }

        return properties;
    }

    public Type getType() {
        String type = System.getProperty(PROPERTY_TYPE_NAME);
        if (type == null) {
            LOGGER.warn("there is no \"{}\" property, used {} type", PROPERTY_TYPE_NAME, DEFAULT_TYPE);
            return DEFAULT_TYPE;
        }

        LOGGER.debug("property \"{}\" exists = {}", PROPERTY_TYPE_NAME, type);

        try {
            return Type.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException exception) {
            LOGGER.error("type value is incorrect = {}, used \"{}\" type", type, DEFAULT_TYPE, exception);
            return DEFAULT_TYPE;
        }
    }
}
